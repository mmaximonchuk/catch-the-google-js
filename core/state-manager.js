import { EVENTS, GAME_STATUSES, MOVING_DIRECTIONS } from "./constants.js";

const _state = {
  gameStatus: GAME_STATUSES.SETTINGS,
  settings: {
    gridSize: {
      rowsCount: 4,
      columnsCount: 4,
    },
    /**
     * In milliseconds
     */
    googleJumpInterval: 2000,
    pointsToLose: 5,
    pointsToWin: 5,
  },
  positions: {
    google: {
      x: 1,
      y: 1,
    },
    players: [
      { x: 0, y: 0 },
      { x: 1, y: 0 },
    ],
  },
  points: {
    google: 0,
    players: [0, 0],
  },
};

let googleJumpIntervalId = null;

export const startGame = async () => {
  if (_state.gameStatus !== GAME_STATUSES.SETTINGS) {
    throw new Error(
      `Can't start game. Game status is not ${GAME_STATUSES.SETTINGS}.`
    );
  }

  _state.positions.players[0] = { x: 0, y: 0 };
  _state.positions.players[1] = {
    x: _state.settings.gridSize.columnsCount - 1,
    y: _state.settings.gridSize.rowsCount - 1,
  };
  _jumpGoogleToNewPosition();

  _state.points.google = 0;
  _state.points.players = [0, 0];

  googleJumpIntervalId = setInterval(() => {
    const prevPosition = { ..._state.positions.google };

    _jumpGoogleToNewPosition();
    _notifyObservers(EVENTS.GOOGLE_RUN_AWAY);
    _notifyObservers(EVENTS.GOOGLE_JUMPED, {
      prevPosition,
      newPosition: { ..._state.positions.google },

    });

    _state.points.google = _state.points.google + 1;
    _notifyObservers(EVENTS.SCORES_CHANGED);

    if (_state.points.google >= _state.settings.pointsToLose) {
      clearInterval(googleJumpIntervalId);
      _state.gameStatus = GAME_STATUSES.LOSE;
      _notifyObservers(EVENTS.STATUS_CHANGED);
    }
  }, _state.settings.googleJumpInterval);

  _state.gameStatus = GAME_STATUSES.IN_PROGRESS;
  _notifyObservers(EVENTS.STATUS_CHANGED);
};

export const playAgain = async () => {
  _state.gameStatus = GAME_STATUSES.SETTINGS;

  _notifyObservers(EVENTS.STATUS_CHANGED);
};

function _getPlayerIndexByNumber(playerNumber) {
  const playerIndex = playerNumber - 1;
  if (playerIndex < 0 || playerIndex > _state.positions.players.length - 1) {
    throw new Error("Invalid player number");
  }

  return playerIndex;
}

const _generetaIntegerNumber = (from, to) => {
  return Math.floor(Math.random() * (to - from) + from);
};

const _jumpGoogleToNewPosition = () => {
  const newPosition = { ..._state.positions.google };

  do {
    newPosition.x = _generetaIntegerNumber(
      0,
      _state.settings.gridSize.columnsCount
    );
    newPosition.y = _generetaIntegerNumber(
      0,
      _state.settings.gridSize.rowsCount
    );

    var isNewPositionMatchWithCurrentGooglePosition =
      newPosition.x === _state.positions.google.x &&
      newPosition.y === _state.positions.google.y;
    var isNewPositionMatchWithCurrentPlayer1Position =
      newPosition.x === _state.positions.players[0].x &&
      newPosition.y === _state.positions.players[0].y;
    var isNewPositionMatchWithCurrentPlayer2Position =
      newPosition.x === _state.positions.players[1].x &&
      newPosition.y === _state.positions.players[1].y;
  } while (
    isNewPositionMatchWithCurrentGooglePosition ||
    isNewPositionMatchWithCurrentPlayer1Position ||
    isNewPositionMatchWithCurrentPlayer2Position
  );

  _state.positions.google = newPosition;
};

// OBSERVER

let _observers = [];

export const _notifyObservers = (name, payload = {}) => {
  const event = {
    name,
    payload,
  };

  _observers.forEach((observer) => {
    try {
      observer(event);
    } catch (error) {
      console.error(error);
    }
  });
};
export const subscribe = (observer) => {
  _observers.push(observer);
};

export const unsubscribe = (observer) => {
  _observers = _observers.filter((obs) => obs !== observer);
};
// INTERFACE/ADAPTER

export const movePlayer = async (playerNumber, direction) => {
  if (_state.gameStatus !== GAME_STATUSES.IN_PROGRESS) {
    console.warn("You can move only when game is in progress");
    return;
  }

  const prevPosition = await getPlayerPosition(playerNumber);

  const newPosition = { ...prevPosition };

  switch (direction) {
    case MOVING_DIRECTIONS.UP: {
      newPosition.y -= 1;
      break;
    }
    case MOVING_DIRECTIONS.DOWN: {
      newPosition.y += 1;
      break;
    }
    case MOVING_DIRECTIONS.LEFT: {
      newPosition.x -= 1;
      break;
    }
    case MOVING_DIRECTIONS.RIGHT: {
      newPosition.x += 1;
      break;
    }
  }

  const isValidRange = _isPositionInValidRange(newPosition);

  if (!isValidRange) return;

  const isPlayer1PositionTheSame =
    _doesPositionMatchWithPlayer1Position(newPosition);
  if (isPlayer1PositionTheSame) return;

  const isPlayer2PositionTheSame =
    _doesPositionMatchWithPlayer2Position(newPosition);
  if (isPlayer2PositionTheSame) return;

  const isGooglePositionTheSame =
    _doesPositionMatchWithGooglePosition(newPosition);
  if (isGooglePositionTheSame) {
    await _catchGoogle(playerNumber);
  }

  _state.positions.players[_getPlayerIndexByNumber(playerNumber)] = newPosition;

  _notifyObservers(EVENTS[`PLAYER${playerNumber}_MOVED`], {
    prevPosition,
    newPosition,
  });
};

// GETTERS/SELECTORS/QUERY

export const _catchGoogle = async (playerNumber) => {
  _state.points.players[_getPlayerIndexByNumber(playerNumber)] =
    _state.points.players[_getPlayerIndexByNumber(playerNumber)] + 1;

  _notifyObservers(EVENTS.SCORES_CHANGED);
  _notifyObservers(EVENTS.GOOGLE_CAUGTH);

  if (
    _state.points.players[_getPlayerIndexByNumber(playerNumber)] ===
    _state.settings.pointsToWin
  ) {
    _state.gameStatus = GAME_STATUSES.WIN;
    clearInterval(_state.timerId);
    _notifyObservers(EVENTS.STATUS_CHANGED);
  } else {
    const prevPosition = await getGooglePosition();
    _jumpGoogleToNewPosition();

    const newPosition = { ..._state.positions.google };

    _notifyObservers(EVENTS.GOOGLE_JUMPED, {
      prevPosition,
      newPosition,
    });
  }
};

export const _doesPositionMatchWithGooglePosition = (position) => {
  return (
    position.x === _state.positions.google.x &&
    position.y === _state.positions.google.y
  );
};

export const _doesPositionMatchWithPlayer1Position = (position) => {
  return (
    position.x === _state.positions.players[0].x &&
    position.y === _state.positions.players[0].y
  );
};

export const _doesPositionMatchWithPlayer2Position = (position) => {
  return (
    position.x === _state.positions.players[1].x &&
    position.y === _state.positions.players[1].y
  );
};
export const _isPositionInValidRange = (position) => {
  if (
    position.x < 0 ||
    position.y < 0 ||
    position.x >= _state.settings.gridSize.columnsCount ||
    position.y >= _state.settings.gridSize.rowsCount
  ) {
    return false;
  }

  return true;
};

export const getGooglePoints = async () => {
  return _state.points.google;
};

/**
 * Retrieves the points of a player based on their player number.
 *
 * @param {number} playerNumber - The number of the player.
 * @return {Promise<number>} The points of the player.
 * @throws {Error} If the player number is invalid.
 */
export const getPlayerPoints = async (playerNumber) => {
  const playerIndex = _getPlayerIndexByNumber(playerNumber);

  return _state.points.players[playerIndex];
};

/**
 * Retrieves the grid size from the state.
 *
 * @return {Promise<{rowsCount: number, columnsCount: number}>} A promise that resolves to an object containing the grid size.
 */
export const getGridSize = async () => {
  return { ..._state.settings.gridSize };
};

/**
 * Retrieves the current position of Google from the state.
 *
 * @return {Promise<{x: number, y: number}>} A promise that resolves to an object containing the current position of Google.
 */
export const getGooglePosition = async () => {
  return { ..._state.positions.google };
};

/**
 * Retrieves the position of a player based on their player number.
 *
 * @param {number} playerNumber - The number of the player.
 * @return {Promise<{x: number, y: number}>} A promise that resolves to an object containing the position of the player.
 * @throws {Error} If the player number is invalid.
 */
export const getPlayerPosition = async (playerNumber) => {
  const playerIndex = _getPlayerIndexByNumber(playerNumber);

  return { ..._state.positions.players[playerIndex] };
};

export const getGameStatuse = async () => {
  return _state.gameStatus;
};
