export const GAME_STATUSES = {
    SETTINGS: "settings",
    IN_PROGRESS: "in-progress",
    WIN: "win",
    LOSE: "lose",
}

export const EVENTS = {
    GOOGLE_JUMPED: "google-jumped",
    PLAYER1_MOVED: "player1-moved",
    PLAYER2_MOVED: "player2-moved",
    STATUS_CHANGED: "status-changed",
    SCORES_CHANGED: "scores-changed",
    GOOGLE_CAUGTH: "google-caugth",
    GOOGLE_RUN_AWAY: "google-run-away",
}

export const MOVING_DIRECTIONS = {
    UP: "up",
    DOWN: "down",
    LEFT: "left",
    RIGHT: "right",
}