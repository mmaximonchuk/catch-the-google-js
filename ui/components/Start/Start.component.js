import { startGame } from "../../../core/state-manager.js";

export const StartComponent = () => {
  const startContainerElement = document.createElement("div");
  startContainerElement.classList.add("startComponent");

  render(startContainerElement);

  return { element: startContainerElement };
};

const render = async (containerElement) => {
  const button = document.createElement("button");
  button.classList.add("startButton");
  button.textContent = "Start Game";

  button.addEventListener("click", () => {
    startGame();
  });

  containerElement.append(button);
};
