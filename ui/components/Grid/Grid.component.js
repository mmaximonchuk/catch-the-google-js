import { MOVING_DIRECTIONS } from "../../../core/constants.js";
import {
  getGridSize,
  movePlayer,
  subscribe,
  unsubscribe,
} from "../../../core/state-manager.js";
import { CellComponent } from "./Cell/Cell.component.js";

export const GridComponent = () => {
  const localState = { cleanUpFunctions: [] };

  const keyUpHandler = (event) => {
    if (event.code === "ArrowUp") {
      movePlayer(1, MOVING_DIRECTIONS.UP);
      return;
    }

    if (event.code === "ArrowDown") {
      movePlayer(1, MOVING_DIRECTIONS.DOWN);
      return;
    }

    if (event.code === "ArrowLeft") {
      movePlayer(1, MOVING_DIRECTIONS.LEFT);
      return;
    }

    if (event.code === "ArrowRight") {
      movePlayer(1, MOVING_DIRECTIONS.RIGHT);
      return;
    }

    if (event.code === "KeyW") {
      movePlayer(2, MOVING_DIRECTIONS.UP);
      return;
    }

    if (event.code === "KeyS") {
      movePlayer(2, MOVING_DIRECTIONS.DOWN);
      return;
    }

    if (event.code === "KeyA") {
      movePlayer(2, MOVING_DIRECTIONS.LEFT);
      return;
    }

    if (event.code === "KeyD") {
      movePlayer(2, MOVING_DIRECTIONS.RIGHT);
      return;
    }
  };

  document.addEventListener("keyup", keyUpHandler);

  const gridContainerElement = document.createElement("div");
  gridContainerElement.classList.add("grid");

  render(gridContainerElement, localState);

  return {
    element: gridContainerElement,
    componentWillUnmount: () => {
      localState.cleanUpFunctions.forEach((cleanUp) => cleanUp());
      document.removeEventListener("keyup", keyUpHandler);
    },
  };
};

const render = async (containerElement, localState) => {
  console.log("grid rendered");
  containerElement.textContent = "";

  localState.cleanUpFunctions.forEach((cleanUp) => cleanUp());
  localState.cleanUpFunctions = [];

  const gridSize = await getGridSize();

  for (let y = 0; y < gridSize.rowsCount; y++) {
    const row = document.createElement("tr");

    row.classList.add("row");

    for (let x = 0; x < gridSize.columnsCount; x++) {
      const { element: cell, componentWillUnmount: cleanUp } = CellComponent(
        x,
        y
      );
      localState.cleanUpFunctions.push(cleanUp);

      row.append(cell);
    }

    containerElement.append(row);
  }
};
