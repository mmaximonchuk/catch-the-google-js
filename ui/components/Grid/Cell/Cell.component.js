import { EVENTS } from "../../../../core/constants.js";
import {
  getGooglePosition,
  getPlayerPosition,
  subscribe,
  unsubscribe,
} from "../../../../core/state-manager.js";
import { GoogleComponent } from "../../common/Google/Google.component.js";
import { PlayerComponent } from "../../common/Player/Player.component.js";

export const CellComponent = (x, y) => {
  const localState = {
    renderVersion: 0,
  };
  const cellElement = document.createElement("td");
  cellElement.classList.add("cell");

  const observer = (event) => {
    if (
      event.name !== EVENTS.GOOGLE_JUMPED &&
      event.name !== EVENTS.PLAYER1_MOVED &&
      event.name !== EVENTS.PLAYER2_MOVED
    )
      return;

    const { prevPosition, newPosition } = event.payload;
    const { x: prevX, y: prevY } = prevPosition;
    const { x: newX, y: newY } = newPosition;

    const shouldCellRender =
      (prevX === x && prevY === y) || (newX === x && newY === y);

    if (!shouldCellRender) return;

    render(cellElement, localState, x, y);
  };

  subscribe(observer);

  render(cellElement, localState, x, y);

  return {
    element: cellElement,
    componentWillUnmount: () => {
      unsubscribe(observer);
    },
  };
};

const render = async (containerElement, localState, x, y) => {
  localState.renderVersion += 1;
  const currentRenderVersion = localState.renderVersion;

  containerElement.textContent = "";
  const googlePosition = await getGooglePosition();
  const getPlayer1Position = await getPlayerPosition(1);
  const getPlayer2Position = await getPlayerPosition(2);

  if (currentRenderVersion < localState.renderVersion) {
    console.log("currentRenderVersion: ", currentRenderVersion);
    console.log("localState.renderVersion: ", localState.renderVersion);

    return;
  }

  if (x === googlePosition.x && y === googlePosition.y) {
    const { element: googleElement } = GoogleComponent();
    containerElement.append(googleElement);
  }

  if (x === getPlayer1Position.x && y === getPlayer1Position.y) {
    const { element: playerElement } = PlayerComponent(1);
    containerElement.append(playerElement);
  }

  if (x === getPlayer2Position.x && y === getPlayer2Position.y) {
    const { element: playerElement } = PlayerComponent(2);
    containerElement.append(playerElement);
  }
};
