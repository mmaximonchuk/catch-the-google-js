export const PlayerComponent = (playerNumber) => {
  const playerElement = document.createElement("img");

  playerElement.classList.add("playerImg");

  render(playerElement, playerNumber);

  return { element: playerElement };
};

const render = async (containerElement, playerNumber) => {
  containerElement.src = `assets/images/player${playerNumber}.png`;
};
