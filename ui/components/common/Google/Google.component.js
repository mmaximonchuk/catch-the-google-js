export const GoogleComponent = () => {
  const googleElement = document.createElement("img");

  googleElement.classList.add("googleImg");

  render(googleElement);

  return { element: googleElement };
};

const render = async (containerElement) => {
  containerElement.src = "assets/images/google.png";
};
