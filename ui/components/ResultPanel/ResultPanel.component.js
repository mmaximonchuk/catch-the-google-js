import { EVENTS } from "../../../core/constants.js";
import {
  getGooglePoints,
  getPlayerPoints,
  subscribe,
} from "../../../core/state-manager.js";

export const ResultPanelComponent = () => {
  const resultPanelContainerElement = document.createElement("div");
  resultPanelContainerElement.classList.add("resultPanel");

  subscribe( (evt) => {
    if (evt.name !== EVENTS.SCORES_CHANGED) return;

    render(resultPanelContainerElement);
  });

  render(resultPanelContainerElement);

  return {
    element: resultPanelContainerElement,
    componentWillUnmount: () => {},
  };
};

const render = async (containerElement) => {
  containerElement.textContent = "";
  const googlePoints = await getGooglePoints();
  const player1Points = await getPlayerPoints(1);
  const player2Points = await getPlayerPoints(2);

  containerElement.append(
    `Player 1: ${player1Points} | Player 2: ${player2Points} | Google: ${googlePoints}`
  );
};
