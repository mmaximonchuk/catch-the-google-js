import { playAgain } from "../../../core/state-manager.js";

export const LoseComponent = () => {
    const loseContainerElement = document.createElement("div");
    loseContainerElement.classList.add("lose");

    render(loseContainerElement);

    return { element: loseContainerElement};
}


const render = async (containerElement) => {
    const titleElement = document.createElement("h1");
    titleElement.classList.add("loseTitle");
    titleElement.textContent = "YOU LOSE!";

    const startAgainButton = document.createElement("button");
    startAgainButton.classList.add("startAgainButton");
    startAgainButton.textContent = "Start Again";


    startAgainButton.addEventListener("click", () => {
        playAgain()
    })

    containerElement.append(titleElement, startAgainButton);
}