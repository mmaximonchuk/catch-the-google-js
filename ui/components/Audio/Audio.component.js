import { EVENTS } from "../../../core/constants.js";
import { subscribe } from "../../../core/state-manager.js";

export const AudioComponent = () => {
  const catchAudio = new Audio("assets/sounds/catch.wav");
  const missAudio = new Audio("assets/sounds/miss.mp3");

  subscribe(({ name }) => {
    if (name === EVENTS.GOOGLE_CAUGTH) {
      catchAudio.currentTime = 0;
      catchAudio.play();
    }
    if (name === EVENTS.GOOGLE_RUN_AWAY) {
      catchAudio.currentTime = 0;
      missAudio.play();
    }
  });
};
