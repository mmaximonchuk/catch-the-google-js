import { GAME_STATUSES } from "../../core/constants.js";
import { getGameStatuse, subscribe } from "../../core/state-manager.js";
import { AudioComponent } from "./Audio/Audio.component.js";
import { GridComponent } from "./Grid/Grid.component.js";
import { LoseComponent } from "./Lose/Lose.component.js";
import { ResultPanelComponent } from "./ResultPanel/ResultPanel.component.js";
import { SettingsComponent } from "./Settings/Settings.component.js";
import { StartComponent } from "./Start/Start.component.js";
import { WinComponent } from "./Win/Win.component.js";

export const AppComponent = () => {
  const localState = { prevGameStatus: null, cleanFunctions: [] };
  const appContainerElement = document.createElement("div");

  const audioComponent = AudioComponent();

  subscribe(() => {
    render(appContainerElement, localState);
  });

  render(appContainerElement, localState);


  return { element: appContainerElement };
};

const render = async (containerElement, localState) => {
  const gameStatus = await getGameStatuse();

  if (localState.prevGameStatus === gameStatus) return;
  containerElement.textContent = "";

  localState.prevGameStatus = gameStatus;
  localState.cleanFunctions.forEach((cleanUp) => cleanUp());
  localState.cleanFunctions = [];


  switch (gameStatus) {
    case GAME_STATUSES.SETTINGS: {
      const settingsComponent = SettingsComponent();
      const startComponent = StartComponent();

      containerElement.append(
        settingsComponent.element,
        startComponent.element
      );
      break;
    }
    case GAME_STATUSES.IN_PROGRESS: {
      const settingsComponent = SettingsComponent();
      const resultPanelComponent = ResultPanelComponent();
      const gridComponent = GridComponent();

      localState.cleanFunctions.push(resultPanelComponent.componentWillUnmount);
      localState.cleanFunctions.push(gridComponent.componentWillUnmount);

      containerElement.append(
        settingsComponent.element,
        resultPanelComponent.element,
        gridComponent.element
      );
      break;
    }
    case GAME_STATUSES.WIN: {
      const winComponent = WinComponent();

      containerElement.append(winComponent.element);
      break;
    }
    case GAME_STATUSES.LOSE: {
      const loseComponent = LoseComponent();

      containerElement.append(loseComponent.element);
      break;
    }
    default:
      throw new Error("Invalid game status. Not implemented.");
  }
};
