export const WinComponent = () => {
  const winContainerElement = document.createElement("div");
  winContainerElement.classList.add("win");

  render(winContainerElement);

  return { element: winContainerElement };
};

const render = async (containerElement) => {
  const title = document.createElement("h1");
  title.classList.add("winTitle");
  title.textContent = "YOU WIN!";
  containerElement.append(title);
};
