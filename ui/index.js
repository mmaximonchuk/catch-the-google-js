import { AppComponent } from "./components/App.component.js";

const rootDiv = document.getElementById("root");

rootDiv.textContent = "";
const appComponent = AppComponent();

rootDiv.append(appComponent.element);
